using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ServiceSaveDataSheet", menuName = "ScriptableObjects/ServiceDataSheet", order = 1)]
public class ServiceSaveDataSheet : ScriptableObject {
    public List<string> servServicesVdo;
    public List<string> localServicesVdo;
}
