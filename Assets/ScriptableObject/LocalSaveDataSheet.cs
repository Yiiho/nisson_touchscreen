using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LocalSaveDataSheet", menuName = "ScriptableObjects/LocalSaveDataSheet", order = 1)]
public class LocalSaveDataSheet : ScriptableObject {
    public string product_name;
    public string id;

    public string servMainPageVdoPath;
    public string localMainPageVdoPath;

    public string servSpecPageImgPath;
    public string localSpecPageImgPath;

    public List<string> servVdoPageAllPath;
    public List<string> localVdoPageAllPath;

    public List<string> servBrochurePageImgPath;
    public List<string> localBrochurePageImgPath;

    public List<string> servPromotionPageImgPath;
    public List<string> localPromotionPageImgPath;

    public string servQrPageImgPath;
    public string localQrPageImgPath;

    public string servScreenSaver;
    public string localScreenSaver;

    public string servQR2;
    public string localQR2;
}
