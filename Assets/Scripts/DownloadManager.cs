using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class DownloadManager : MonoBehaviour {
    public delegate void Downloader();
    public event Downloader OnDownloadCompleted;
    LocalSaveDataSheet sheetData;
    ServiceSaveDataSheet serviceSheet;
    enum DownloadProgress { none, main_vdo, spec, qr, screensaver, qr2, promotion, brochure, vdo, service }
    // Start is called before the first frame update
    void Start() {
        sheetData = GlobalManager.Instance.localSaver.saveSheet;
        serviceSheet = GlobalManager.Instance.localSaver.serviceSheet;
        ClearSheetData();
    }

    private void ClearSheetData() {
        sheetData.servVdoPageAllPath.Clear();
        sheetData.localVdoPageAllPath.Clear();

        sheetData.servBrochurePageImgPath.Clear();
        sheetData.localBrochurePageImgPath.Clear();

        sheetData.servPromotionPageImgPath.Clear();
        sheetData.localPromotionPageImgPath.Clear();
    }

    int countIndex;
    int maxCount;
    public void DownloadService() {
        currentProgress = DownloadProgress.service;
        _folderName = "service";
        maxCount = serviceSheet.servServicesVdo.Count;
        countIndex = 0;
        serviceSheet.localServicesVdo.Clear();
        serviceSheet.localServicesVdo = new List<string>();
        StartCoroutine(DownloadFile(serviceSheet.servServicesVdo[countIndex]));
    }

    private void OnProgressiveService() {
        countIndex++;
        if (countIndex == maxCount) {
            if (OnDownloadCompleted != null) OnDownloadCompleted();
        } else {
            serviceSheet.localServicesVdo.Add(fileSavedPath);
            StartCoroutine(DownloadFile(serviceSheet.servServicesVdo[countIndex]));
        }
    }

    DownloadProgress currentProgress;
    EnqueueDownloadData _queue;
    string _folderName;
    public void DownloadRequests(EnqueueDownloadData queue, string folderName) {
        _folderName = folderName;
        Debug.Log("DownloadRequests Queue >>> " + folderName);
        ClearSheetData();
        currentProgress = DownloadProgress.none;
        _queue = queue;
        CheckProgress();
    }

    private void CheckProgress() {
        switch(currentProgress) {
            case DownloadProgress.none: 
                currentProgress = DownloadProgress.main_vdo;
                StartCoroutine(DownloadFile(_queue.mainVdo));
                break;
            case DownloadProgress.main_vdo:
                sheetData.servMainPageVdoPath = downloadPath;
                sheetData.localMainPageVdoPath = fileSavedPath;
                currentProgress = DownloadProgress.spec;
                StartCoroutine(DownloadFile(_queue.spec));
                break;
            case DownloadProgress.spec:
                sheetData.servSpecPageImgPath = downloadPath;
                sheetData.localSpecPageImgPath = fileSavedPath;
                currentProgress = DownloadProgress.qr;
                StartCoroutine(DownloadFile(_queue.qr));
                break;
            case DownloadProgress.qr:
                sheetData.servQrPageImgPath = downloadPath;
                sheetData.localQrPageImgPath = fileSavedPath;
                currentProgress = DownloadProgress.screensaver;
                StartCoroutine(DownloadFile(_queue.screenSaver));
                break;
            case DownloadProgress.screensaver:
                sheetData.servScreenSaver = downloadPath;
                sheetData.localScreenSaver = fileSavedPath;
                currentProgress = DownloadProgress.qr2;
                StartCoroutine(DownloadFile(_queue.qr2));
                break;
            case DownloadProgress.qr2:
                sheetData.servQR2 = downloadPath;
                sheetData.localQR2 = fileSavedPath;
             //   currentProgress = DownloadProgress.promotion;
             //   StartCoroutine(DownloadFile(_queue.promotion));
                DownloadBrochureAndVDO();
                break;
            case DownloadProgress.promotion:
             //   sheetData.servPromotionPageImgPath = downloadPath;
             //   sheetData.localPromotionPageImgPath = fileSavedPath;
                DownloadBrochureAndVDO();
                break;
            case DownloadProgress.brochure:
                DownloadBrochureAndVDO();
                break;
            case DownloadProgress.vdo:
                DownloadBrochureAndVDO();                
                break;
            case DownloadProgress.service:
                OnProgressiveService();
                break;
        }
        Debug.Log("On CheckProgress at task " + currentProgress.ToString());
    }

    private void DownloadBrochureAndVDO() {
        countIndex++;
        if(currentProgress == DownloadProgress.qr2) {
            currentProgress = DownloadProgress.promotion;
            countIndex = 0;
            maxCount = _queue.promotion.Count;
            if (maxCount > 0) {
                StartCoroutine(DownloadFile(_queue.promotion[countIndex]));
            } else {
                downloadPath = "";
                fileSavedPath = "";
                CheckProgress();
            }
        } else if(currentProgress == DownloadProgress.promotion) {
            sheetData.servPromotionPageImgPath.Add(downloadPath);
            sheetData.localPromotionPageImgPath.Add(fileSavedPath);

            if (countIndex >= maxCount) {
                currentProgress = DownloadProgress.brochure;
                countIndex = 0;
                maxCount = _queue.brochure.Count;
                if (maxCount > 0) {
                    StartCoroutine(DownloadFile(_queue.brochure[countIndex]));
                } else {
                    downloadPath = "";
                    fileSavedPath = "";
                    CheckProgress();
                }
            } else {
                StartCoroutine(DownloadFile(_queue.promotion[countIndex]));
            }
        } else if(currentProgress == DownloadProgress.brochure) {
            sheetData.servBrochurePageImgPath.Add(downloadPath);
            sheetData.localBrochurePageImgPath.Add(fileSavedPath);
            print("+++++++++++++++++++ Download Brochure at " + countIndex + "/" + maxCount);
            print("+++++++++++++++++++ sheetData.localBrochurePageImgPath at "+ downloadPath + " <<<<< length " + sheetData.servBrochurePageImgPath.Count);
            if (countIndex >= maxCount) {
                countIndex = 0;
                maxCount = _queue.vdoFile.Count;
                currentProgress = DownloadProgress.vdo;
                if (maxCount > 0) {
                    StartCoroutine(DownloadFile(_queue.vdoFile[countIndex]));
                } else {
                    downloadPath = "";
                    fileSavedPath = "";
                    CheckProgress();
                }
            } else {
                StartCoroutine(DownloadFile(_queue.brochure[countIndex]));
            }
        }else if(currentProgress == DownloadProgress.vdo) {
            sheetData.servVdoPageAllPath.Add(downloadPath);
            sheetData.localVdoPageAllPath.Add(fileSavedPath);
            if (countIndex >= maxCount) {
                if (OnDownloadCompleted != null) OnDownloadCompleted();
            } else {
                StartCoroutine(DownloadFile(_queue.vdoFile[countIndex]));
            }
        }

    }

    string downloadPath;
    string fileSavedPath;

    IEnumerator DownloadFile(string _url) {
        if (String.IsNullOrEmpty(_url)) {
            downloadPath = "";
            fileSavedPath = "";
            yield return null;
        } else {
            downloadPath = _url;
            int index = (_url.LastIndexOf("/") + 1);
            string file_name = _url.Substring(index, _url.Length - index);
            string savePath = Path.Combine(Application.persistentDataPath, _folderName + "/" + file_name);
            savePath = SetStringSlashUnityFormat(savePath, false);
            Debug.Log("file extension == " + Path.GetExtension(savePath));
            if (Path.GetExtension(savePath) != "") {
                UnityWebRequest www = new UnityWebRequest(_url, UnityWebRequest.kHttpVerbGET);
                Debug.Log("REQ >> " + _url);
                www.downloadHandler = new DownloadHandlerFile(savePath);
                yield return www.SendWebRequest();
                if (www.isNetworkError || www.isHttpError) {
                    Debug.Log("error >> " + www.error);
                    fileSavedPath = "";
                } else {
                    Debug.Log("File successfully downloaded and saved to " + savePath);
                    fileSavedPath = savePath;
                }
            } else {
                fileSavedPath = "";
                yield return null;
            }

        }
        CheckProgress();
    }

    string SetStringSlashUnityFormat(string s, bool setQuoteIfBlank = true) {
        s = s.Replace(@"\", "/");
        if (setQuoteIfBlank) s = s.Replace("\"", "");
        return s;
    }
}
