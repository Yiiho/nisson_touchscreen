using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.Networking;
using System;

public class FetchJsonData : MonoBehaviour {
    public LocalSave localSaver;
    public Text textServ;
    public Text debugTxt;
   // DownloadManager dlManager;
    FileInfo[] info;
    bool isFetching = false;
    int dataCateVdoTemp = -1;
    int dataCateBrochureTemp = -1;
    int dataCatePromotionTemp = -1;

    public void LoadJsonFromServ() {
        if (isFetching) return;
        isFetching = true;
        Debug.Log("LoadJsonFromServ >> " + textServ.text);
        debugTxt.text = "LoadJsonFromServ >> " + textServ.text;
        dataCateVdoTemp = -1;
        dataCateBrochureTemp = -1;
        dataCatePromotionTemp = -1;
        StartCoroutine(GetJson(textServ.text));
    }

    IEnumerator GetJson(string url) {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError) {
            Debug.Log("Network error >> " + www.isNetworkError);
            debugTxt.text = "\n"+"Download Json Network error.";
        } else {
            // Show results as text
            Debug.Log(www.downloadHandler.text);
            debugTxt.text = "\n" + "Get Json completed.";
            // Or retrieve results as binary data
            string results = www.downloadHandler.text;
            OnDownloadFileCompleted(results);
        }
    }

    private void OnDownloadFileCompleted(string jsonString) {
        DoSimpleJson(jsonString);
    }

    JSONNode nodeData;
    int indexJson = 0;
    private void DoSimpleJson(string s) {//Read JSON.
        Debug.Log(s);
        nodeData = SimpleJSON.JSON.Parse(s);
        debugTxt.text += "\n" + "Start Checking JSON data.";
        //debugTxt.text += "\n" + "Start Checking JSON product indexing 0/" + nodeData["Items"].Count;
        //Debug.Log("Start Checking JSON product indexing 0 / " + nodeData["Items"].Count);

        // LoadDataByJsonNode(0);

        for (int i = 0; i < nodeData["Items"].Count; i++) {
            print(nodeData["Items"][i]["product_name"]);
            if (localSaver.ProductID == SetStringSlashUnityFormat(nodeData["Items"][i]["product_name"])) {
                indexJson = i;
                CheckJsonByIndex(); //To do here.
                return; //return before  LoadNewDefaultData();
            }
        }

        LoadNewDefaultData(); //Fetch all and change localSaver.ProductID to none;
    }

    private void CheckJsonByIndex() {
        CheckCate(DataCate.main);
    }
    int currentIndex = 0;
    private void CheckCate(DataCate cate) {
        switch (cate) {
            case DataCate.main:
                if (localSaver.saveSheet.servMainPageVdoPath == SetStringSlashUnityFormat(nodeData["Items"][indexJson]["main_vdo"])) {
                    CheckCate(DataCate.spec);
                } else {
                    StartCoroutine(DownloadFile(cate, nodeData["Items"][indexJson]["main_vdo"], localSaver.saveSheet.product_name));
                    debugTxt.text += "\n" + "Update Main vdo";
                }
                break;
            case DataCate.spec:
                if (localSaver.saveSheet.servSpecPageImgPath == SetStringSlashUnityFormat(nodeData["Items"][indexJson]["spec"])) {
                    currentIndex = 0;
                    if (localSaver.saveSheet.servPromotionPageImgPath.Count > nodeData["Items"][indexJson]["promotion_img"].Count) {
                        int amount = localSaver.saveSheet.servPromotionPageImgPath.Count - nodeData["Items"][indexJson]["promotion_img"].Count;
                        localSaver.saveSheet.servPromotionPageImgPath.RemoveRange(localSaver.saveSheet.servPromotionPageImgPath.Count - amount, amount);
                        localSaver.saveSheet.localPromotionPageImgPath.RemoveRange(localSaver.saveSheet.localPromotionPageImgPath.Count - amount, amount);
                    } else if (localSaver.saveSheet.servPromotionPageImgPath.Count < nodeData["Items"][indexJson]["promotion_img"].Count) {
                        for (int i = localSaver.saveSheet.servPromotionPageImgPath.Count; i < nodeData["Items"][indexJson]["promotion_img"].Count; i++) {
                            localSaver.saveSheet.servPromotionPageImgPath.Add("");
                            localSaver.saveSheet.localPromotionPageImgPath.Add("");
                        }
                    }
                    CheckCate(DataCate.promo);
                } else {
                    StartCoroutine(DownloadFile(cate, nodeData["Items"][indexJson]["spec"], localSaver.saveSheet.product_name));
                    debugTxt.text += "\n" + "Update Spec sheet";
                }
                break;
            case DataCate.promo:
                dataCatePromotionTemp = currentIndex;
                print("promo length >> " + nodeData["Items"][indexJson]["promotion_img"].Count);
                print("CheckCate >> promo >> " + currentIndex);
                if (currentIndex < nodeData["Items"][indexJson]["promotion_img"].Count) {
                    if (localSaver.saveSheet.servPromotionPageImgPath[currentIndex] != SetStringSlashUnityFormat(nodeData["Items"][indexJson]["promotion_img"][currentIndex][0])) {
                        debugTxt.text += "\n" + "Update promo";
                        print("Donwload this promo >>>>>> " + nodeData["Items"][indexJson]["promotion_img"][currentIndex][0]);
                        StartCoroutine(DownloadFile(cate, nodeData["Items"][indexJson]["promotion_img"][currentIndex][0], localSaver.saveSheet.product_name));
                    } else {
                        currentIndex++;
                        CheckCate(DataCate.promo);
                    }
                } else { 
                    currentIndex = 0;
                    CheckCate(DataCate.qr);
                }
                /* if (localSaver.saveSheet.servPromotionPageImgPath == SetStringSlashUnityFormat(nodeData["Items"][indexJson]["promotion"])) {
                     CheckCate(DataCate.qr);
                 } else {
                    StartCoroutine(DownloadFile(cate, nodeData["Items"][indexJson]["promotion"], localSaver.saveSheet.product_name));
                    debugTxt.text += "\n" + "Update promotion";
                }*/
                break;
            case DataCate.qr:
                if (localSaver.saveSheet.servQrPageImgPath == SetStringSlashUnityFormat(nodeData["Items"][indexJson]["qr"])) {
                    CheckCate(DataCate.screen);
                } else {
                    StartCoroutine(DownloadFile(cate, nodeData["Items"][indexJson]["qr"], localSaver.saveSheet.product_name));
                    debugTxt.text += "\n" + "Update qr";
                }
                break;
            case DataCate.screen:
                if (localSaver.saveSheet.servScreenSaver == SetStringSlashUnityFormat(nodeData["Items"][indexJson]["screen_vdo"])) {
                    CheckCate(DataCate.qr2);
                } else {
                    StartCoroutine(DownloadFile(cate, nodeData["Items"][indexJson]["screen_vdo"], localSaver.saveSheet.product_name));
                    debugTxt.text += "\n" + "Update screen saver";
                }
                break;
            case DataCate.qr2:
                if (localSaver.saveSheet.servQR2 == SetStringSlashUnityFormat(nodeData["Items"][indexJson]["qr2"])) {
                    currentIndex = 0;
                    if (localSaver.saveSheet.servVdoPageAllPath.Count > nodeData["Items"][indexJson]["vdo"].Count) {
                        int amount = localSaver.saveSheet.servVdoPageAllPath.Count - nodeData["Items"][indexJson]["vdo"].Count;
                        localSaver.saveSheet.servVdoPageAllPath.RemoveRange(localSaver.saveSheet.servVdoPageAllPath.Count - amount, amount);
                        localSaver.saveSheet.localVdoPageAllPath.RemoveRange(localSaver.saveSheet.localVdoPageAllPath.Count - amount, amount);
                    }else if(localSaver.saveSheet.servVdoPageAllPath.Count < nodeData["Items"][indexJson]["vdo"].Count) {
                        for (int i = localSaver.saveSheet.servVdoPageAllPath.Count; i < nodeData["Items"][indexJson]["vdo"].Count; i++) {
                            localSaver.saveSheet.servVdoPageAllPath.Add("");
                            localSaver.saveSheet.localVdoPageAllPath.Add("");
                        }
                    }
                    CheckCate(DataCate.vdos);
                } else {
                    StartCoroutine(DownloadFile(cate, nodeData["Items"][indexJson]["qr2"], localSaver.saveSheet.product_name));
                    Debug.Log("Update qr2");
                    debugTxt.text += "\n" + "Update qr2";
                }
                break;
            case DataCate.vdos:
                if (currentIndex == dataCateVdoTemp) return;
                dataCateVdoTemp = currentIndex;
                print("CheckCate >> vdo >> " + currentIndex );
                if(currentIndex < nodeData["Items"][indexJson]["vdo"].Count) {
                    if (localSaver.saveSheet.servVdoPageAllPath[currentIndex] != SetStringSlashUnityFormat(nodeData["Items"][indexJson]["vdo"][currentIndex][0])) {
                        debugTxt.text += "\n" + "Update vdos";
                        StartCoroutine(DownloadFile(cate, nodeData["Items"][indexJson]["vdo"][currentIndex][0], localSaver.saveSheet.product_name));
                    } else {
                        currentIndex++;
                        CheckCate(DataCate.vdos);
                    }
                } else {
                    currentIndex = 0;
                    if (localSaver.saveSheet.servBrochurePageImgPath.Count > nodeData["Items"][indexJson]["brochure"].Count) {
                        int amount = localSaver.saveSheet.servBrochurePageImgPath.Count - nodeData["Items"][indexJson]["brochure"].Count;
                        localSaver.saveSheet.servBrochurePageImgPath.RemoveRange(localSaver.saveSheet.servBrochurePageImgPath.Count - amount, amount);
                        localSaver.saveSheet.localBrochurePageImgPath.RemoveRange(localSaver.saveSheet.localBrochurePageImgPath.Count - amount, amount);
                    } else if (localSaver.saveSheet.servBrochurePageImgPath.Count < nodeData["Items"][indexJson]["brochure"].Count) {
                        for (int i = localSaver.saveSheet.servBrochurePageImgPath.Count; i < nodeData["Items"][indexJson]["brochure"].Count; i++) {
                            localSaver.saveSheet.servBrochurePageImgPath.Add("");
                            localSaver.saveSheet.localBrochurePageImgPath.Add("");
                        }
                    }
                    CheckCate(DataCate.brochure);
                }
                break;
            case DataCate.brochure:
                if (currentIndex == dataCateBrochureTemp) return;
                dataCateBrochureTemp = currentIndex;
                print("CheckCate >> brochure >> " + currentIndex + "/"+ nodeData["Items"][indexJson]["brochure"].Count);
                if (currentIndex < nodeData["Items"][indexJson]["brochure"].Count) {
                    print("bc in 1");
                    if (localSaver.saveSheet.servBrochurePageImgPath[currentIndex] != SetStringSlashUnityFormat(nodeData["Items"][indexJson]["brochure"][currentIndex][0])) {
                        print("bc in 2");
                        debugTxt.text += "\n" + "Update brochure";
                        StartCoroutine(DownloadFile(cate, nodeData["Items"][indexJson]["brochure"][currentIndex][0], localSaver.saveSheet.product_name));
                    } else {
                        print("bc in 3");
                        currentIndex++;
                        CheckCate(DataCate.brochure);
                    }
                } else {
                    print("bc in 4");
                    OnCheckJsonCompleted();
                }
                break;
            case DataCate.service:
                
                break;
        }
    }

    private void OnCheckJsonCompleted() {
        debugTxt.text += "\n" + "Fetch new json completed.";
        localSaver.SaveDataToLocalJson();

        GlobalManager.Instance.OnLoadLocalSaveCompleted(false);

        isFetching = false;
    }

    enum DataCate { main, spec, promo, qr, screen, qr2, vdos, brochure, service } 

    private void LoadNewDefaultData() {

        GlobalManager.Instance.IsUseDefaultData = true;
        isFetching = false;
    }

    IEnumerator DownloadFile(DataCate cate, string _url, string _folderName) {
        string fileSavedPath;
        string fileServUrl = _url;
        if (String.IsNullOrEmpty(_url)) {
            fileSavedPath = "";
            fileServUrl = "";
           // SaveNewValue(cate, fileSavedPath, fileServUrl);
            yield return null;
            CheckSaveAndStartNewSession(cate, fileSavedPath, fileServUrl);
        } else {
            int index = (_url.LastIndexOf("/") + 1);
            string file_name = _url.Substring(index, _url.Length - index);
            string savePath = Path.Combine(Application.persistentDataPath, _folderName + "/" + file_name);
            savePath = SetStringSlashUnityFormat(savePath, false);
            Debug.Log("file extension == " + Path.GetExtension(savePath));
            if (Path.GetExtension(savePath) != "") {
                UnityWebRequest www = new UnityWebRequest(_url, UnityWebRequest.kHttpVerbGET);
                Debug.Log("REQ >> " + _url);
                www.downloadHandler = new DownloadHandlerFile(savePath);
                yield return www.SendWebRequest();
                if (www.isNetworkError || www.isHttpError) {
                    Debug.Log("error >> " + www.error);
                    fileSavedPath = "";
                } else {
                    Debug.Log("File successfully downloaded and saved to " + savePath);
                    fileSavedPath = savePath;
                  //  currentIndex++;
                  //  CheckCate(cate);
                }
                CheckSaveAndStartNewSession(cate, fileSavedPath, fileServUrl);
            } else {
                fileSavedPath = "";
                yield return null;
                CheckSaveAndStartNewSession(cate, fileSavedPath, fileServUrl);
            }
           // SaveNewValue(cate, fileSavedPath, _url);
        }
       // Debug.Log("Download completed...." + cate.ToString());
      //  currentIndex++;
      // CheckCate(cate);




    }

    void CheckSaveAndStartNewSession(DataCate cate, string fileSavedPath, string _url) {
        SaveNewValue(cate, fileSavedPath, _url);

        currentIndex++;
        CheckCate(cate);
    }

    private void SaveNewValue(DataCate cate, string fileSavedPath, string fileServUrl) {
        print("save new value");
        switch (cate) {
            case DataCate.main:
                localSaver.saveSheet.servMainPageVdoPath = fileServUrl;
                localSaver.saveSheet.localMainPageVdoPath = fileSavedPath;
                break;
            case DataCate.spec:
                localSaver.saveSheet.servSpecPageImgPath = fileServUrl;
                localSaver.saveSheet.localSpecPageImgPath = fileSavedPath;
                break;
            case DataCate.promo:
                localSaver.saveSheet.servPromotionPageImgPath[currentIndex] = fileServUrl;
                localSaver.saveSheet.localPromotionPageImgPath[currentIndex] = fileSavedPath;
                break;
            case DataCate.qr:
                localSaver.saveSheet.servQrPageImgPath = fileServUrl;
                localSaver.saveSheet.localQrPageImgPath = fileSavedPath;
                break;
            case DataCate.screen:
                localSaver.saveSheet.servScreenSaver = fileServUrl;
                localSaver.saveSheet.localScreenSaver = fileSavedPath;
                break;
            case DataCate.qr2:
                localSaver.saveSheet.servQR2 = fileServUrl;
                localSaver.saveSheet.localQR2 = fileSavedPath;
                break;
            case DataCate.vdos:
                print("_i  " + " -----------------------------------------------------------------------------"+currentIndex);
                localSaver.saveSheet.servVdoPageAllPath[currentIndex] = fileServUrl;
                localSaver.saveSheet.localVdoPageAllPath[currentIndex] = fileSavedPath;
                break;
            case DataCate.brochure:
                localSaver.saveSheet.servBrochurePageImgPath[currentIndex] = fileServUrl;
                localSaver.saveSheet.localBrochurePageImgPath[currentIndex] = fileSavedPath;
                break;
            case DataCate.service:

                break;
        }
    }

    string SetStringSlashUnityFormat(string s, bool setQuoteIfBlank = true) {
        if (string.IsNullOrEmpty(s)) {
            return "";
        } else {
            s = s.Replace(@"\", "/");
            if (setQuoteIfBlank) s = s.Replace("\"", "");
            return s;
        }
    }

    public void Clear() {
        info = null;
        nodeData = null;
        System.GC.Collect();
        Resources.UnloadUnusedAssets();
    }
}
