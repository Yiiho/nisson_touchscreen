using UnityEngine;
using UnityEngine.UI;

public class SettingManager : MonoBehaviour {
    public GameObject settingPanel;
    public Text debugPanel;
    public int maxTabCount = 10;
    public float tabCountLengthTime = .25f;
    public InputField url;
    public InputField productId;
    public InputField interval;
    GlobalManager global;

    void Start() {
        global = GlobalManager.Instance;
    }

    int tabCounter;
    float tabTimeStamp;
    public void OnOpenPanel(bool isOpen) {
        if (isOpen) {
            if ((Time.timeSinceLevelLoad - tabTimeStamp) < tabCountLengthTime) {
                tabCounter++;
                //print(tabCounter);
                if(tabCounter >= maxTabCount) {
                    tabCounter = 0;
                    settingPanel.SetActive(true);
                    url.text = global.GetServerUrl();
                    productId.text = global.localSaver.ProductID;
                    interval.text = global.MaxInterval.ToString();
                    debugPanel.gameObject.SetActive(true);
                }
            } else {
                tabCounter = 0;
            }
            tabTimeStamp = Time.timeSinceLevelLoad;
        } else {
            settingPanel.SetActive(false);
            debugPanel.gameObject.SetActive(false);
        }
    }

    public void UpdateProductId() {
        global.localSaver.SaveProductId( productId.text);
    }

    public void UpdateIntervalTimer() {
       // global.MaxInterval = float.Parse(interval.text);
        global.localSaver.SaveInterval(float.Parse(interval.text));
        //global.localSaver.SaveDataToLocalJson();
    }
}
