using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;

public class PromotionManager : EachSceneManager {
    public Transform parent;
    public RawImage prefab;
    public float moveRange = 1282;
    public float speedTransition = .25f;
    int index;
    bool isMoving = false;
    protected override void Start() {
        base.Start();
        index = 0;
        for(int i = 0; i < global.AllAd.Count; i++) {
            RawImage rImg = Instantiate(prefab, parent);
            rImg.texture = global.AllAd[i];
        }
    }

    public void OnPress(string s) {
        if (isMoving) return;
        isMoving = true;
        if (s == "n") {
            if (index >= global.AllAd.Count - 1) {                
                parent.transform.DOLocalMoveX(0-moveRange/2, speedTransition).OnComplete(() => isMoving = false);
                index = 0;
            } else { 
                parent.transform.DOLocalMoveX(parent.transform.localPosition.x - moveRange, speedTransition).OnComplete(() => isMoving = false);
                index++;
            }
        } else {
            if (index <= 0) {
                parent.transform.DOLocalMoveX(parent.transform.localPosition.x - (moveRange*(global.AllAd.Count - 1)), speedTransition).OnComplete(() => isMoving = false);
                index = global.AllAd.Count - 1;
            } else {
                parent.transform.DOLocalMoveX(parent.transform.localPosition.x + moveRange, speedTransition).OnComplete(() => isMoving = false);
                index--;
            }
        }
    }

    public override void Clear() {
        base.Clear();
    }
}
