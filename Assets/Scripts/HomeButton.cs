using UnityEngine;

public class HomeButton : MonoBehaviour {
    public void GoHome() {
        GlobalManager.Instance.ChangeScene(2);
    }
}
