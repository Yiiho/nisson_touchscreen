using UnityEngine;
using UnityEngine.UI;

public class QR_Downloader : MonoBehaviour {
    public RawImage qrImg;
    public GameObject ImageGroup;

    private void Start() {
        qrImg.texture = GlobalManager.Instance.QR2;
    }

    public void Toggler(bool isShow) {
        ImageGroup.SetActive(isShow);
    }

    
}
