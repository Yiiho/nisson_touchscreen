using UnityEngine.UI;
using UnityEngine.Video;
using DG.Tweening;
using System.IO;
using System;

public class MainPageManager : EachSceneManager {
    public Button[] buttons;
    public Image[] deactiveButtons;
    public int startID = 3;
    public VideoPlayer vPlayer;
    public LocalSaveDataSheet dataSheet;
    public ServiceSaveDataSheet serviceSheet;

    protected override void Start() {
        base.Start();
        int i = startID;
        foreach (Button b in buttons) {
            int _i = i;
            b.onClick.AddListener(() => OnToggleId(_i));
            CheckPageHasData(_i);
            i += 1;
        }
        if (global.IsUseDefaultData) {
            vPlayer.source = VideoSource.VideoClip;
        } else {
            print(Path.GetExtension(global.MainVdo) + " ex");
            if (Path.GetExtension(global.MainVdo) != ".mp4") {
                vPlayer.source = VideoSource.VideoClip;
            } else {
                vPlayer.source = VideoSource.Url;
                vPlayer.url = global.MainVdo;
            }
        }
        vPlayer.Play();
    }

    private void CheckPageHasData(int i) {
        bool b; //True = has data.
        int buttonIndex = i - startID;
        if (i == startID) { //spec
            b = IsHasFileContain(dataSheet.localSpecPageImgPath);
        } else if (i == startID + 1) { //vdos
            if (dataSheet.localVdoPageAllPath.Count > 0) {
                b = true;
            } else {
                b = false;
            }
        } else if (i == startID + 2) { //brochure
            if (dataSheet.localBrochurePageImgPath.Count > 0) {
                b = true;
            } else {
                b = false;
            }
        } else if (i == startID + 3) { //promotion
            if (dataSheet.localPromotionPageImgPath.Count > 0) {
                b = true;
            } else {
                b = false;
            }
        } else if (i == startID + 4) { //service
            if (serviceSheet.localServicesVdo.Count > 0) {
                b = true;
            } else {
                b = false;
            }
        } else { //qr
            b = IsHasFileContain(dataSheet.localQrPageImgPath);
        }
        buttons[buttonIndex].gameObject.SetActive(b);
        deactiveButtons[buttonIndex].gameObject.SetActive(!b);
    }

    private void OnToggleId(int id) {
        global.ChangeScene(id);
    }

    private bool IsHasFileContain(string path) {
        if (string.IsNullOrEmpty(path)) {
            return false;
        } else if (Path.GetExtension(path) == "") {
            return false;
        } else if (path == "null") {
            return false;
        } else {
            return true;
        }
    }

    public override void Clear() {
        base.Clear();
        vPlayer.Stop();
        vPlayer.targetTexture.Release();

        int i = startID;
        foreach (Button b in buttons) {
            int _i = i;
            b.onClick.RemoveListener(() => OnToggleId(_i));
            i += 1;
        }
    }
}
