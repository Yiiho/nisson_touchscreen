using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using DG.Tweening;

public class ServiceManager : EachSceneManager {
   // public RawImage img;
    public VideoPlayer vPlayer;
    int currentIndex = 0;
    public VideoClip[] defaultVdo;
   // public Texture2D[] defaultBG;

    protected override void Start() {
        base.Start();
        global.IsUpdateInterval = false;
        DOVirtual.DelayedCall(110, () => global.ChangeScene(1));
        if (global.IsUseDefaultData) {
            print("if");
         //   img.texture = defaultBG[currentIndex];
            vPlayer.source = VideoSource.VideoClip;
            vPlayer.clip = defaultVdo[currentIndex];
            vPlayer.Play();
            vPlayer.isLooping = false;
            vPlayer.loopPointReached += OnDefaultCompleted;
        } else {
            print("else");
         //   img.texture = global.ServiceData[currentIndex].img;
            vPlayer.source = VideoSource.Url;
            vPlayer.url = global.ServiceData[currentIndex];
            vPlayer.Play();
            vPlayer.isLooping = false;
            vPlayer.loopPointReached += OnVideoCompleted;
        }
    }

    private void OnDefaultCompleted(VideoPlayer source) {
        vPlayer.Stop();
        currentIndex = (currentIndex < defaultVdo.Length - 1) ? currentIndex += 1 : 0;
     //   img.texture = defaultBG[currentIndex];
        vPlayer.clip = defaultVdo[currentIndex];
        vPlayer.Play();
    }

    private void OnVideoCompleted(VideoPlayer source) {
        print("sssssssssssssssssss");
        vPlayer.Stop();
        currentIndex = (currentIndex < global.ServiceData.Count - 1) ? currentIndex += 1 : 0;
        //    img.texture = global.ServiceData[currentIndex].img;
        if (string.IsNullOrEmpty(global.ServiceData[currentIndex])) {
            currentIndex = 0;
        }else if(Path.GetExtension(global.ServiceData[currentIndex]) != ".mp4") {
            currentIndex = 0;
        } else if(global.ServiceData[currentIndex] == "null") {
            currentIndex = 0;
        }
 
        vPlayer.url = global.ServiceData[currentIndex];
        vPlayer.Play();
        
    }

    public override void Clear() {
        base.Clear();
        global.IsUpdateInterval = true;
        vPlayer.Stop();
        vPlayer.targetTexture.Release();
        if (global.IsUseDefaultData) {
            vPlayer.loopPointReached -= OnDefaultCompleted;
        } else {
            vPlayer.loopPointReached -= OnVideoCompleted;
        }
    }
}
