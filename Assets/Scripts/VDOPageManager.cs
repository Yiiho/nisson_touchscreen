using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class VDOPageManager : MonoBehaviour {
    public VideoPlayer vPlayer;
    public GameObject thmbVdoSeek;
    Button[] buttonsSeek;
    public int startFramePercent = 5;
    public int increaseRatePercent = 8;
    public List<ThumbnailSeek> listThumbnail;
    void Start() {
        vPlayer.audioOutputMode = VideoAudioOutputMode.None;
        vPlayer.renderMode = VideoRenderMode.APIOnly;
        
        buttonsSeek = thmbVdoSeek.GetComponentsInChildren<Button>();
        listThumbnail = new List<ThumbnailSeek>();
        int vdoLength = Convert.ToInt32(vPlayer.clip.frameCount);
        print(vdoLength);
        for(int i = 0; i < buttonsSeek.Length; i++) {
            ThumbnailSeek ts = new ThumbnailSeek();
            int _i = i;
            buttonsSeek[i].onClick.AddListener(() => OnToggleId(_i));
            ts.frame = GetFrameByPercentage(startFramePercent + (increaseRatePercent * i), vdoLength);
           // print(ts.frame+ " ts.frame "+ i);
            listThumbnail.Add(ts);
        }

        //CreateThumbnail();
        //vPlayer.clip.see

        PrepareVideoForPlayback();
    }

    private void OnToggleId(int id) {
        Seek(listThumbnail[id].frame, true);
    }

    int index = 0;

    private void StartThumbnail() {
        vPlayer.seekCompleted += OnSeekCompleted;
        CreateThumbnail();
    }
    private void CreateThumbnail() {
        Seek(listThumbnail[index].frame, false);
        
    }

    void OnSeekCompleted(VideoPlayer source) { 

    }

    int GetFrameByPercentage(int percent, int maxFrame) {
        float f = ((float)percent / 100f) * (float)maxFrame;
        return Mathf.CeilToInt(f);
    }

    void Seek(int frame, bool playAfterSeek) {
        vPlayer.frame = frame;
        if (playAfterSeek) {
            vPlayer.Play();
        } else { 
            vPlayer.Pause(); 
        }
    }


    public void PrepareVideoForPlayback() {
      //  vPlayer.url = media.path;
      //  vPlayer.source = VideoSource.Url;
        vPlayer.renderMode = VideoRenderMode.APIOnly;
        vPlayer.prepareCompleted += OnVideoPrepareCompleted;
        vPlayer.Prepare();
    }

    void OnVideoPrepareCompleted(VideoPlayer source) {
        vPlayer.prepareCompleted -= OnVideoPrepareCompleted;
        vPlayer.sendFrameReadyEvents = true;
        vPlayer.seekCompleted += OnVideoSeekCompleted;


        vPlayer.frame = listThumbnail[index].frame;
        /*var middleFrame = (int)(vPlayer.frameCount / 2);
        if (middleFrame > 0) vPlayer.frame = middleFrame;*/
    }

    void SeekAfterPrepared() {
        print("SeekAfterPrepared");
        vPlayer.seekCompleted += OnVideoSeekCompleted;
        vPlayer.frame = listThumbnail[index].frame;
    }

    void OnVideoSeekCompleted(VideoPlayer source) {
        vPlayer.seekCompleted -= OnVideoSeekCompleted;
        vPlayer.frameReady += OnVideoPreviewFrameReady;
        vPlayer.SetDirectAudioMute(trackIndex: 0, mute: true);
        vPlayer.Play();
    }

    void OnVideoPreviewFrameReady(VideoPlayer source, long frameIdx) {
        vPlayer.frameReady -= OnVideoPreviewFrameReady;
        vPlayer.Pause();


        buttonsSeek[index].image.sprite = ConvertToSprite(GetRTPixels(source.texture as RenderTexture));
        vPlayer.frame = 0;
        vPlayer.SetDirectAudioMute(trackIndex: 0, mute: false);
      //  OnVideoPreviewTextureExtracted();



        index++;
        if(index < buttonsSeek.Length) {
            print(index);
            SeekAfterPrepared();
        } else {
            vPlayer.renderMode = VideoRenderMode.RenderTexture;
            vPlayer.audioOutputMode = VideoAudioOutputMode.Direct;
            Seek(0, true);
        }
    }
    /*
    void ExtractVideoPreview(VideoPlayer source) {
        Benchmark("Extracting preview texture");
        RenderTexture renderTexture = source.texture as RenderTexture;
        media.texture = new Texture2D(renderTexture.width, renderTexture.height);
        RenderTexture.active = renderTexture;
        media.texture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        media.texture.Apply();
        RenderTexture.active = null;

    }*/

    public Texture2D GetRTPixels(RenderTexture rt) {
        // Remember currently active render texture
        RenderTexture currentActiveRT = RenderTexture.active;

        // Set the supplied RenderTexture as the active one
        RenderTexture.active = rt;

        // Create a new Texture2D and read the RenderTexture image into it
        Texture2D tex = new Texture2D(rt.width, rt.height);
        tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);

        tex.Apply();
        // Restorie previously active render texture
        RenderTexture.active = null;
        return tex;
    }

    Sprite ConvertToSprite(Texture2D texture) {
        return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
    }
}

[System.Serializable]
public struct ThumbnailSeek {
    public int frame;
    public Sprite thumbnail;
}
