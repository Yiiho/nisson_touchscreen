using UnityEngine;
using SimpleJSON;
using System.IO;
using System;
using System.Collections.Generic;

public class LocalSave : MonoBehaviour {
    public LocalSaveDataSheet saveSheet;
    public ServiceSaveDataSheet serviceSheet;
    GlobalManager global;

    const string _productID = "productID";
    const string _serverUrl = "serverUrl";
    const string _intervalTime = "intervalTime";

    private void Start() {
        LoadProductId();
        LoadServerUrl();
        LoadInterval();

        global = GlobalManager.Instance;


        if(ProductID == "") {
            ProductID = "none";
            SaveProductId(ProductID);
            global.OnLoadLocalSaveCompleted(true); //true cuz create new save.

           // print("in this <<<< ");
        } else {
            LoadFromLocalJson(ProductID);            
        } 
    }

    #region load, save, get PlayerPrefs >> ProductID
    public void SaveProductId(string id) {
        ProductID = id;
        PlayerPrefs.SetString(_productID, id);
        PlayerPrefs.Save();

        LoadFromLocalJson(id);
    }

    public string ProductID {
        get; private set;
    }

    public void LoadProductId() {      
        if (PlayerPrefs.HasKey(_productID)) {           
            ProductID = PlayerPrefs.GetString(_productID);
        } else {
            ProductID = "";
        //    SaveProductId(ProductID);
        }
    }
    #endregion

    #region load, save, get PlayerPrefs >> ServerUrl
    public void SaveServerUrl(string url) {
        PlayerPrefs.SetString(_serverUrl, url);
        PlayerPrefs.Save();
    }

    public string ServerUrl { get; set; }

    public void LoadServerUrl() {
        if (PlayerPrefs.HasKey(_serverUrl)) {
            ServerUrl = PlayerPrefs.GetString(_serverUrl);
        } else {
            ServerUrl = "https://monstersdev.com/api/view_api_nissan";
            SaveServerUrl(ServerUrl);
        }

    }
    #endregion

    #region load, save, get PlayerPrefs >> IntervalTime
    public void SaveInterval(float tt) {
        print("save interval " + tt);
        PlayerPrefs.SetFloat(_intervalTime, tt);
        PlayerPrefs.Save();

        if(GlobalManager.Instance.MaxInterval  != tt) {
            GlobalManager.Instance.MaxInterval = tt;
        }
    }

    public float IntervalTime { get; set; }

    public void LoadInterval() {
        if (PlayerPrefs.HasKey(_intervalTime)) {
            print("hsa interval");
            IntervalTime = PlayerPrefs.GetFloat(_intervalTime);
        } else {
            print("not have interval");
            IntervalTime = 50;
        }
        GlobalManager.Instance.MaxInterval = IntervalTime;
    }
    #endregion

    public void SaveDataToLocalJson() { //Convert string to Json then save.
        JSONClass save = new JSONClass();

        //product id
        save.Add(nameof(saveSheet.id), saveSheet.id);

        //product name
        save.Add(nameof(saveSheet.product_name), saveSheet.product_name);

        //main page vdo
        save.Add(nameof(saveSheet.servMainPageVdoPath), saveSheet.servMainPageVdoPath);
        save.Add(nameof(saveSheet.localMainPageVdoPath), saveSheet.localMainPageVdoPath);

        //spec page img
        save.Add(nameof(saveSheet.servSpecPageImgPath), saveSheet.servSpecPageImgPath);
        save.Add(nameof(saveSheet.localSpecPageImgPath), saveSheet.localSpecPageImgPath);

        //vdo page img and vdo
        JSONArray servAllVdoView = new JSONArray();
        foreach(string s in saveSheet.servVdoPageAllPath) {
            servAllVdoView.Add(s);
        }
        save.Add(nameof(saveSheet.servVdoPageAllPath), servAllVdoView);

        JSONArray localAllVdoView = new JSONArray();
        foreach(string s in saveSheet.localVdoPageAllPath) {
            localAllVdoView.Add(s);
        }
        save.Add(nameof(saveSheet.localVdoPageAllPath), localAllVdoView);

        //brochure page imgs
        JSONArray servBrochure = new JSONArray();
        foreach (string s in saveSheet.servBrochurePageImgPath) {
            servBrochure.Add(s);
        }
        save.Add(nameof(saveSheet.servBrochurePageImgPath), servBrochure);
        JSONArray localBrochure = new JSONArray();
        foreach(string s in saveSheet.localBrochurePageImgPath) {
            localBrochure.Add(s);
        }
        save.Add(nameof(saveSheet.localBrochurePageImgPath), localBrochure);

        //promotion page vdo
        JSONArray servPromo = new JSONArray();
        foreach (string s in saveSheet.servPromotionPageImgPath) {
            servPromo.Add(s);
        }
        save.Add(nameof(saveSheet.servPromotionPageImgPath), servPromo);
        JSONArray localPromo = new JSONArray();
        foreach (string s in saveSheet.localPromotionPageImgPath) {
            localPromo.Add(s);
        }
        save.Add(nameof(saveSheet.localPromotionPageImgPath), localPromo);

        //qr page img
        save.Add(nameof(saveSheet.servQrPageImgPath), saveSheet.servQrPageImgPath);
        save.Add(nameof(saveSheet.localQrPageImgPath), saveSheet.localQrPageImgPath);

        //screen saver
        save.Add(nameof(saveSheet.servScreenSaver), saveSheet.servScreenSaver);
        save.Add(nameof(saveSheet.localScreenSaver), saveSheet.localScreenSaver);

        //qr2
        save.Add(nameof(saveSheet.servQR2), saveSheet.servQR2);
        save.Add(nameof(saveSheet.localQR2), saveSheet.localQR2);

        //Debug.Log(save.ToString());

        string path = Application.persistentDataPath + "/" + saveSheet.product_name + ".json";
        File.WriteAllText(path, save.ToString());
    }

    public void LoadFromLocalJson(string productName) { //load json path.
        LoadJsonFile(Application.persistentDataPath + "/" + productName + ".json");
    }

    private void LoadJsonFile(string path) {
        if (File.Exists(path)) {
            string s = File.ReadAllText(path);
            global.IsUseDefaultData = false;
            DoLoadLocalSaveData(s);
        } else {//Set default data if it not have save data.
            global.IsUseDefaultData = true;
        }
        global.OnLoadLocalSaveCompleted(false); //false cuz it has save data.
    }

    private void DoLoadLocalSaveData(string s) {//Convert Json to string and contain to scriptableobject.
        JSONNode jsonNode = SimpleJSON.JSON.Parse(s);

        saveSheet.id = jsonNode[nameof(saveSheet.id)];
        saveSheet.product_name = jsonNode[nameof(saveSheet.product_name)];

        saveSheet.servMainPageVdoPath = jsonNode[nameof(saveSheet.servMainPageVdoPath)];
        saveSheet.localMainPageVdoPath = jsonNode[nameof(saveSheet.localMainPageVdoPath)];

        saveSheet.servSpecPageImgPath = jsonNode[nameof(saveSheet.servSpecPageImgPath)];
        saveSheet.localSpecPageImgPath = jsonNode[nameof(saveSheet.localSpecPageImgPath)];

        //vdo page
        saveSheet.servVdoPageAllPath.Clear();
        saveSheet.localVdoPageAllPath.Clear();
        for (int i = 0; i < jsonNode[nameof(saveSheet.localVdoPageAllPath)].Count; i++) {
            int _i = i;
            saveSheet.localVdoPageAllPath.Add(jsonNode[nameof(saveSheet.localVdoPageAllPath)][_i]);
            saveSheet.servVdoPageAllPath.Add(jsonNode[nameof(saveSheet.servVdoPageAllPath)][_i]);
        }



        //brochure page
        saveSheet.localBrochurePageImgPath.Clear();
        saveSheet.servBrochurePageImgPath.Clear();
        for (int j = 0; j < jsonNode[nameof(saveSheet.localBrochurePageImgPath)].Count; j++) {
            int _j = j;
            saveSheet.servBrochurePageImgPath.Add(jsonNode[nameof(saveSheet.servBrochurePageImgPath)][_j]);
            saveSheet.localBrochurePageImgPath.Add(jsonNode[nameof(saveSheet.localBrochurePageImgPath)][_j]);
        }

        //promotion page
        saveSheet.servPromotionPageImgPath.Clear();
        saveSheet.localPromotionPageImgPath.Clear();
        for (int k = 0; k < jsonNode[nameof(saveSheet.localPromotionPageImgPath)].Count; k++) {
            int _k = k;
            saveSheet.servPromotionPageImgPath.Add(jsonNode[nameof(saveSheet.servPromotionPageImgPath)][_k]);
            saveSheet.localPromotionPageImgPath.Add(jsonNode[nameof(saveSheet.localPromotionPageImgPath)][_k]);
        }


        //qr page
        saveSheet.servQrPageImgPath = jsonNode[nameof(saveSheet.servQrPageImgPath)];
        saveSheet.localQrPageImgPath = jsonNode[nameof(saveSheet.localQrPageImgPath)];


        //screen saver
        saveSheet.servScreenSaver = jsonNode[nameof(saveSheet.servScreenSaver)];
        saveSheet.localScreenSaver = jsonNode[nameof(saveSheet.localScreenSaver)];

        //qr2
        saveSheet.servQR2 = jsonNode[nameof(saveSheet.servQR2)];
        saveSheet.localQR2 = jsonNode[nameof(saveSheet.localQR2)];

        //Debug.Log("Loaded local save Json.");

        LoadJsonService();
    }

    public void SaveJsonService() {
        JSONClass save = new JSONClass();

        JSONArray servService = new JSONArray();        
        foreach (string s in serviceSheet.servServicesVdo) {
            servService.Add(s);
        }
        save.Add("server", servService);

        JSONArray localService = new JSONArray();
        foreach (string l in serviceSheet.localServicesVdo) {
            localService.Add(l);
        }
        save.Add("local", localService);

        string path = Application.persistentDataPath + "/service.json";
        File.WriteAllText(path, save.ToString());
    }

    private void LoadJsonService() {
        string path = Application.persistentDataPath + "/service.json";
        string s = File.ReadAllText(path);

        serviceSheet.servServicesVdo.Clear();
        serviceSheet.localServicesVdo.Clear();
        serviceSheet.servServicesVdo = new List<string>();
        serviceSheet.localServicesVdo = new List<string>();

        JSONNode jsonNode = SimpleJSON.JSON.Parse(s);
        for(int i =0; i < jsonNode["server"].Count; i++) {
            serviceSheet.servServicesVdo.Add(jsonNode["server"][i]);            
        }
        for(int j=0;  j < jsonNode["local"].Count; j++) {
            serviceSheet.localServicesVdo.Add(jsonNode["local"][j]);
        }


        //Debug.Log("Loaded local service Json.");

        global.OnLoadLocalSaveCompleted(false);
    }
}
