using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.IO;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;
using DG.Tweening;

public class JsonReader : MonoBehaviour {
    public delegate void ReadJson();
    public event ReadJson OnJsonReaded;
    //public string wwwPath;
    public LocalSaveDataSheet saveSheet;
    public ServiceSaveDataSheet serviceSheet;
    public Text txt;
    public DownloadManager dlManager;
    enum CurrentProcess { LoadJsonFile, LoadFileFromJson }

    GlobalManager global;
    bool IsForceUpdate { get; set; }
    void Start() {
        global = GlobalManager.Instance;
    }

    public void UpdateData(bool isForceUpdate) { //If forceUpdate = Load all data from Json. Else skip load.
        IsForceUpdate = isForceUpdate;
        //print(Application.persistentDataPath);
        if (IsForceUpdate) {
            LoadJsonFromServ(global.GetServerUrl()); //True way <<<<
        } else {
            ManageDataToGlobal();
        }
    }

    private void ManageDataToGlobal() {
        ProcessMainPageFile();
    }

    private void LoadJsonFromServ(string serverUrl) {
        Debug.Log("LoadJsonFromServ >> " + serverUrl);
        StartCoroutine(GetJson(serverUrl));
        //DownloadFile(serverUrl, ".json", "Nissan", CurrentProcess.LoadJsonFile);
    }

    IEnumerator GetJson(string url) {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError) {
            Debug.Log("Network error >> " + www.isNetworkError);
            txt.text = "Download Json Network error.";
        } else {
            // Show results as text
            Debug.Log(www.downloadHandler.text);

            // Or retrieve results as binary data
            string results = www.downloadHandler.text;
            OnDownloadFileCompleted(results);
        }
    }

    private void OnDownloadFileCompleted(string jsonString) {
        Debug.Log(" OnDownloadFileCompleted " + IsForceUpdate);
        if (IsForceUpdate) {
           // string savePath = Path.Combine(Application.persistentDataPath, "Nissan.json");
            DoSimpleJson(jsonString);
            string activeID = global.localSaver.ProductID;
            if (activeID == "none") {
                global.IsUseDefaultData = true;
             //   global.ChangeScene(1);
            } else {
                // use product by id.
            }
        } else {
            if (saveSheet.id == "none") {
                saveSheet.id = "0";
                global.localSaver.SaveDataToLocalJson();
            }
         //   LoadFileLocalById(saveSheet.id);
        }
    }

    int cNode;
    JSONNode nodeData;
    private void DoSimpleJson(string s) {
        //Read JSON.
        print(s);
        cNode = 0;
        nodeData = SimpleJSON.JSON.Parse(s);
        txt.text += "\n" + "Start Download file "+ cNode+" / " + nodeData["Items"].Count;
        Debug.Log("Start Download file " + cNode +" / " + nodeData["Items"].Count);

        LoadDataByJsonNode();
    }

    private void LoadDataByJsonNode() {
        if (cNode < nodeData["Items"].Count) {
            DoDownload();
        } else {
            DownloadService();
            
            //   Debug.Log("Download completed.");
            //  global.ChangeScene(1);
        }
    }

    private void DownloadService() {
        txt.text += "\n" + "Download Services data.";
        serviceSheet.servServicesVdo.Clear();
        serviceSheet.servServicesVdo = new List<string>();
        print(nodeData["service"][0]);
        print(nodeData["service"][0].Count + " count");
        for (int i = 0; i < nodeData["service"][0].Count; i++) {
            serviceSheet.servServicesVdo.Add(nodeData["service"][0][i]);
        }
        dlManager.DownloadService();
        dlManager.OnDownloadCompleted += LoadServiceCompleted; 
    }

    private void LoadServiceCompleted() {
        dlManager.OnDownloadCompleted -= LoadServiceCompleted;
        global.localSaver.SaveJsonService();
        Debug.Log("Download completed.");
        txt.text += "\n" + "Download completed.";
        txt.text += "\n" + "Wait to refresh data....";
        DOVirtual.DelayedCall(1.5f, () => global.ChangeScene(1));
    }

    void DoDownload() {
        saveSheet.id = nodeData["Items"][cNode]["id"];
        saveSheet.product_name = nodeData["Items"][cNode]["product_name"];
        EnqueueDownloadData downloadData = new EnqueueDownloadData();
        downloadData.mainVdo = nodeData["Items"][cNode]["main_vdo"];
        downloadData.spec = nodeData["Items"][cNode]["spec"];
        //downloadData.promotion = nodeData["Items"][cNode]["promotion"];
        downloadData.qr = nodeData["Items"][cNode]["qr"];
        downloadData.screenSaver = nodeData["Items"][cNode]["screen_vdo"];
        downloadData.qr2 = nodeData["Items"][cNode]["qr2"];

        downloadData.brochure = new List<string>();
        for(int i = 0; i < nodeData["Items"][cNode]["brochure"].Count; i++) {
            downloadData.brochure.Add(nodeData["Items"][cNode]["brochure"][i][0]);
        }

        downloadData.vdoFile = new List<string>();
        for(int j = 0; j < nodeData["Items"][cNode]["vdo"].Count; j++) {
            downloadData.vdoFile.Add(nodeData["Items"][cNode]["vdo"][j][0]);
            print(" <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    " + nodeData["Items"][cNode]["vdo"][j][0]);
        }

        downloadData.promotion = new List<string>();
        for(int k = 0; k < nodeData["Items"][cNode]["promotion_img"].Count; k++) {
            downloadData.promotion.Add(nodeData["Items"][cNode]["promotion_img"][k][0]);
        }

        dlManager.DownloadRequests(downloadData, saveSheet.product_name);

        dlManager.OnDownloadCompleted += CheckLengthDownloadData;
    }

    private void CheckLengthDownloadData() {
        dlManager.OnDownloadCompleted -= CheckLengthDownloadData;
        global.DoSaveJsonLocal();
        cNode++;
        txt.text += "\n" + "Start Download file "+cNode+"/" + nodeData["Items"].Count;

        DOVirtual.DelayedCall(2, LoadDataByJsonNode); //wait to write json.
    }

    private void ProcessMainPageFile() {
        GlobalManager.Instance.MainVdo = saveSheet.localMainPageVdoPath;
        ProcessSpecSheet();
    }

    private void ProcessSpecSheet() {
        if (saveSheet.localSpecPageImgPath != "") {
            GlobalManager.Instance.SpecTexture = LoadTexture(saveSheet.localSpecPageImgPath);
        }
        ProcessVdoPageFiles();
    }


    private void ProcessVdoPageFiles() {
        List<string> listDummy = new List<string>();
        for (int i = 0; i < saveSheet.localVdoPageAllPath.Count; i++) {
            listDummy.Add(saveSheet.localVdoPageAllPath[i]);
        }
        GlobalManager.Instance.AllVdoView = listDummy;
        ProcessPdfSheet();
    }


    private void ProcessPdfSheet() {
        List<Texture2D> t2ds = new List<Texture2D>();
        for (int i = 0; i < saveSheet.localBrochurePageImgPath.Count; i++) { //mock for pdf length.
            Texture2D t2d = LoadTexture(saveSheet.localBrochurePageImgPath[i]);
            t2ds.Add(t2d);
        }
        GlobalManager.Instance.AllPdfImg = t2ds;
        ProcessAd();
    }
    private void ProcessAd() {
        //GlobalManager.Instance.AllAd = saveSheet.localPromotionPageImgPath;
        List<Texture2D> t2ds = new List<Texture2D>();
        for(int i = 0; i < saveSheet.localPromotionPageImgPath.Count; i++) {
            Texture2D t2d = LoadTexture(saveSheet.localPromotionPageImgPath[i]);
            t2ds.Add(t2d);
        }
        GlobalManager.Instance.AllAd = t2ds;
        ProcessService();
    }

    private void ProcessService() {
        List<string> ss = new List<string>();
        for (int i = 0; i < serviceSheet.localServicesVdo.Count; i++) {
            ss.Add(serviceSheet.localServicesVdo[i]);
        }
        GlobalManager.Instance.ServiceData = ss;
        ProcessQrCode();
    }

    private void ProcessQrCode() {
        if (saveSheet.localQrPageImgPath!="") {
            GlobalManager.Instance.QrTexture = LoadTexture(saveSheet.localQrPageImgPath);
        }

        ProcessScreenSaver();

        //global.ChangeScene(1);
    }

    private void ProcessScreenSaver() {
        GlobalManager.Instance.ScreenSaver = saveSheet.localScreenSaver;

        ProcessQR2();
    }

    private void ProcessQR2() {
        if (saveSheet.localQR2 != "") {
            GlobalManager.Instance.QR2 = LoadTexture(saveSheet.localQR2);
        }

        global.ChangeScene(1);
    }

    public static Texture2D LoadTexture(string filePath) {
        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath)) {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }

    #region Create t2d and QR
    public Texture2D generateQR(string text) {
        var encoded = new Texture2D(256, 256);
        var color32 = Encode(text, encoded.width, encoded.height);
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }

    private static Color32[] Encode(string textForEncoding, int width, int height) {
        var writer = new BarcodeWriter {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }
    #endregion
}

public struct EnqueueDownloadData {
    public string mainVdo;
    public string spec;
    public List<string> vdoFile;
    public List<string> brochure;
    public List<string> promotion;
    public string qr;
    public string screenSaver;
    public string qr2;
}