using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class DragObject : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    public bool isDragable { get; set; }
    public Vector2 min { get; set; }
    public Vector2 max { get; set; }

    Vector3 offset;
    
    void Start() {
        isDragable = true;
    }

    public void OnBeginDrag(PointerEventData eventData) {
        if (isDragable) {
            offset = Input.mousePosition - transform.position;
        }
    }

    public void OnDrag(PointerEventData eventData) {
        if (isDragable) {
            transform.position = (Input.mousePosition - offset);
        } else {
            OnEndDrag(eventData);
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        if(transform.position.x < min.x || transform.position.x > max.x) {
            float target = (transform.position.x < min.x) ? min.x: max.x;
            transform.DOMoveX(target, .5f);
        }

        if (transform.position.y < min.y || transform.position.y > max.y) {
            float target = (transform.position.y < min.y) ? min.y : max.y;
            transform.DOMoveY(target, .5f);
        }
    }

    
}
