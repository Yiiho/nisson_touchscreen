
using System.IO;
using UnityEngine.Video;

public class ScreenSaverManager : EachSceneManager {
    public VideoPlayer vPlayer;
    protected override void Start() {
        base.Start();

        if (global.IsUseDefaultData) {
            vPlayer.source = VideoSource.VideoClip;
        } else {
            print(Path.GetExtension(global.ScreenSaver+" ex"));
            if (Path.GetExtension(global.ScreenSaver) != ".mp4") {
                print(" on nottttt");
                vPlayer.source = VideoSource.VideoClip;
            } else {
                print(" oh yes");
                vPlayer.source = VideoSource.Url;
                vPlayer.url = global.ScreenSaver;
            }           
        }
        vPlayer.Play();
    }
    public void OnClick() {
        global.ChangeScene(2);
    }


    public override void Clear() {
        base.Clear();
        vPlayer.Stop();
        vPlayer.targetTexture.Release();
    }
}
