using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using System;

public class Vdo9VID : EachSceneManager {
    public VideoPlayer vPlayer;
    public GameObject parentButton;
    public long frameCap = 15;
    Button[] buttons;

    int vdoLength;
    int index;
    bool useDefaultData;
    protected override void Start() {
        base.Start();
        global.IsUpdateInterval = false;
        buttons = parentButton.GetComponentsInChildren<Button>();

        useDefaultData = global.IsUseDefaultData;
        if (global.AllVdoView.Count < 1) { useDefaultData = true; }


        if (useDefaultData) {
            vdoLength = 1;
        } else {
            vdoLength = global.AllVdoView.Count;
        }
        index = 0;
        CaptureTmp();

        vPlayer.loopPointReached += OnVdoEnded;
        //PrepareVideoForPlayback();
    }

    private void OnVdoEnded(VideoPlayer source) {
        global.IsUpdateInterval = true;
    }

    void CaptureTmp() {
        vPlayer.audioOutputMode = VideoAudioOutputMode.None;
        vPlayer.renderMode = VideoRenderMode.APIOnly;

        if (useDefaultData) {
            vPlayer.source = VideoSource.VideoClip;
        } else {
            vPlayer.source = VideoSource.Url;
            vPlayer.url = global.AllVdoView[index];
        }
        vPlayer.prepareCompleted += OnVideoPrepareCompleted;
        vPlayer.Prepare();
    }

    void PlayDefaultVDO() {
        if (vPlayer.isPlaying) {
            vPlayer.Stop();
        }
        vPlayer.source = VideoSource.VideoClip;
        vPlayer.Play();
    }

    void OnToggleVdo(string path) {
        if (vPlayer.isPlaying) {
            vPlayer.Stop();
            vPlayer.targetTexture.Release();
        }
        vPlayer.source = VideoSource.Url;
        vPlayer.url = path;
        vPlayer.Play();
        global.IsUpdateInterval = false;
    }

    /*

    public void PrepareVideoForPlayback() {
        vPlayer.renderMode = VideoRenderMode.APIOnly;
        vPlayer.prepareCompleted += OnVideoPrepareCompleted;
        vPlayer.Prepare();
    }*/

    void OnVideoPrepareCompleted(VideoPlayer source) {
        vPlayer.prepareCompleted -= OnVideoPrepareCompleted;
        vPlayer.sendFrameReadyEvents = true;
        vPlayer.seekCompleted += OnVideoSeekCompleted;


        vPlayer.frame = frameCap;
    }

    void SeekAfterPrepared() {
        print("SeekAfterPrepared");
        vPlayer.seekCompleted += OnVideoSeekCompleted;
        vPlayer.frame = frameCap;
    }

    void OnVideoSeekCompleted(VideoPlayer source) {
        vPlayer.seekCompleted -= OnVideoSeekCompleted;
        vPlayer.frameReady += OnVideoPreviewFrameReady;
        vPlayer.SetDirectAudioMute(trackIndex: 0, mute: true);
        vPlayer.Play();
    }

    void OnVideoPreviewFrameReady(VideoPlayer source, long frameIdx) {
        Debug.Log("OnVideoPreviewFrameReady  >> ");
        vPlayer.frameReady -= OnVideoPreviewFrameReady;
        vPlayer.Pause();

        RawImage ri = buttons[index].GetComponent<RawImage>();
        ri.texture = GetRTPixels(source.texture as RenderTexture);
        ri.color = Color.white;
        vPlayer.frame = 0;
        vPlayer.SetDirectAudioMute(trackIndex: 0, mute: false);

        index++;
        if (index < vdoLength) {
            print(index+"----- index");
            if (!useDefaultData) {
                vPlayer.source = VideoSource.Url;
                vPlayer.url = global.AllVdoView[index];
                ///SeekAfterPrepared();


                CaptureTmp();
            } else {
                vPlayer.renderMode = VideoRenderMode.RenderTexture;
                vPlayer.audioOutputMode = VideoAudioOutputMode.Direct;

                PlayVdo();
            }
           
        } else {
            vPlayer.renderMode = VideoRenderMode.RenderTexture;
            vPlayer.audioOutputMode = VideoAudioOutputMode.Direct;

            PlayVdo();
        }
        Debug.Log("OnVideoPreviewFrameReady  <<< ");
    }

    /*
    void Seek(int frame, bool playAfterSeek) {
        vPlayer.frame = frame;
        if (playAfterSeek) {
            vPlayer.Play();
        } else {
            vPlayer.Pause();
        }
    }
    */
    void PlayVdo() {
        print("PlayVdo   >>>> ");
        if (useDefaultData) {
            print("use default");
            for (int i = 0; i < buttons.Length; i++) {
                print(i);
                if (i < vdoLength) {
                    int _i = i;
                    buttons[i].onClick.AddListener(PlayDefaultVDO);
                  //  buttons[i].GetComponent<RawImage>().texture = t2ds[_i];
                } else {
                    buttons[i].gameObject.SetActive(false);
                }
            }
            PlayDefaultVDO();
        } else {
            List<string> allVdoView = global.AllVdoView;
            for (int i = 0; i < buttons.Length; i++) {
                if (i < allVdoView.Count) {
                    int _i = i;
                    buttons[i].onClick.AddListener(() => OnToggleVdo(allVdoView[_i]));
                } else {
                    buttons[i].gameObject.SetActive(false);
                }
            }
            OnToggleVdo(allVdoView[0]);
        }
        print("PlayVdo   <<<<< ");
    }



    public override void Clear() {
        base.Clear();
        vPlayer.loopPointReached -= OnVdoEnded;
        global.IsUpdateInterval = true;
        vPlayer.Stop();
        vPlayer.targetTexture.Release();

        if (useDefaultData) {
            for (int i = 0; i < buttons.Length; i++) {
                if (i < vdoLength) {
                    buttons[i].onClick.RemoveListener(PlayDefaultVDO);
                    buttons[i].GetComponent<RawImage>().texture = null;
                }
            }
        } else {
            for (int i = 0; i < vdoLength; i++) {
                int _i = i;
                buttons[i].onClick.RemoveListener(() => OnToggleVdo(global.AllVdoView[_i]));
                buttons[i].GetComponent<RawImage>().texture = null;
            }
        }
    }



    public Texture2D GetRTPixels(RenderTexture rt) {
        RenderTexture currentActiveRT = RenderTexture.active;

        RenderTexture.active = rt;
        Texture2D tex = new Texture2D(rt.width, rt.height);
        tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);

        tex.Apply();
        RenderTexture.active = null;
        return tex;
    }

}
