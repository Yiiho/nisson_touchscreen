using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class GlobalManager : MonoBehaviour {
    public static GlobalManager Instance;
    public JsonReader jReader;
    public LocalSave localSaver;
    public Image fadeImage;
    public Text debugText;
    //public FetchJsonData loadNewData;
    public GameObject settingPanel;
    private void Awake() {
        if (Instance == null) Instance = this;
        DontDestroyOnLoad(this);
    }

    private void Start() {
        Application.targetFrameRate = 60;
        IsUpdateInterval = true;
    }
    float intervalCounter = 0;
    
    void Update() {
        if (IsUpdateInterval) {
            if (MaxInterval > 0) { //if interval == 0 or lower not countdown Interval timer.
                if (Input.GetMouseButton(0)) {
                    intervalCounter = 0;
                } else {
                    intervalCounter += Time.deltaTime;
                    if (intervalCounter > MaxInterval) {
                        intervalCounter = 0;
                        if (SceneManager.GetActiveScene().buildIndex != 0 || SceneManager.GetActiveScene().buildIndex != 1) {
                            ChangeScene(1);
                        }
                    }
                }
            }
        }
    }

    public bool IsUpdateInterval { get; set; }

    public string GetKeyProductId {
        get {
           return localSaver.ProductID;
        } 
        
    }
    public string GetServerUrl(){
        return localSaver.ServerUrl;
    }

    public float MaxInterval { get; set; }

    public bool IsUseDefaultData { get; set; }

    public void OnLoadLocalSaveCompleted(bool createNewSave) { //Initial process after loaded PlayerPref data.
        print("OnLoadLocalSaveCompleted  >> " + createNewSave);
        jReader.UpdateData(createNewSave);
    }

    public void LoadBackendData(bool isForceUpdate) { //Load data from url.
        if (isForceUpdate) {
            jReader.UpdateData(isForceUpdate);
        }
    }


    public void OnLoadBackendCompleted() {
        localSaver.SaveDataToLocalJson();
       // ChangeScene(1);
    }

    EachSceneManager pageManager;
    public void ChangeScene(int id) { //Fade scene then change scene.
            debugText.gameObject.SetActive(settingPanel.activeSelf);
            fadeImage.color = new Color(1, 1, 1, 0);
            fadeImage.gameObject.SetActive(true);
            fadeImage.DOFade(1, .3f).OnComplete(() => {
                if (pageManager != null) pageManager.Clear();
                IsUpdateInterval = true;
                SceneManager.LoadScene(id);
                fadeImage.DOFade(0, .3f).OnComplete(() => {
                    fadeImage.gameObject.SetActive(false);
                    if (TryGetComponent<EachSceneManager>(out var eachSceneManager)) {
                        pageManager = eachSceneManager;
                    } else {
                        pageManager = null;
                    }
                });
            });
    }

    public string ScreenSaver { get; set; }

    public Texture2D QR2 { get; set; }

    public string AppVersion { get; set; }

    public string MainVdo { get; set; }

    public List<string> AllVdoView { get; set; }

    public Texture2D SpecTexture { get; set; }

    public List<Texture2D> AllPdfImg{ get; set; }

    public List<Texture2D> AllAd { get; set; }

    public List<string> ServiceData { get; set; }

    public Texture2D QrTexture { get; set; }


    public void DoSaveJsonLocal() {
        localSaver.SaveDataToLocalJson();
    }

}

[System.Serializable]
public struct VdoAndImg {
    public string vdoPath;
    public Texture2D img;
}

[System.Serializable]
public struct VdoAndImgPaths {
    public string vdoPath;
    public string img;
}

