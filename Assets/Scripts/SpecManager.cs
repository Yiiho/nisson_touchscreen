using UnityEngine;
using UnityEngine.UI;

public class SpecManager : EachSceneManager {
    public RectTransform image;
    public Texture2D defaultTexture;
    public float zoomMin = 1;
    public float zoomMax = 2;
    public float lerpTime = .5f;
    public float doubleTabDelay = .25f;

    Vector3 touchStart;
    float tabTimeStamp;
    Vector2 moveMin = new Vector2();
    Vector2 moveMax = new Vector2();

    protected override void Start() {
        base.Start();
        if (global.IsUseDefaultData) {
            image.GetComponent<RawImage>().texture = defaultTexture;
        } else {
            image.GetComponent<RawImage>().texture = global.SpecTexture;
        }
        moveMin.x = 0;
        moveMin.y = 0;
        moveMax.x = Screen.width;
        moveMax.y = Screen.height;
    }

    private void Update() {
        if (Input.GetMouseButtonDown(0)) { //Start tab and checking double tab evnt.
            if ((Time.timeSinceLevelLoad - tabTimeStamp) < doubleTabDelay) {
                DoDoubleTabEvent();
            }
            tabTimeStamp = Time.timeSinceLevelLoad;
            touchStart = Input.mousePosition - image.position;
        }
        if (Input.touchCount == 2) { //Checking pinch evnt.
            touch1 = Input.GetTouch(0);
            touch2 = Input.GetTouch(1);

            if (touch1.phase == TouchPhase.Began && touch2.phase == TouchPhase.Began) {
                lastDist = Vector2.Distance(touch1.position, touch2.position);
            }
            if (touch1.phase == TouchPhase.Moved && touch2.phase == TouchPhase.Moved) {
                float newDist = Vector2.Distance(touch1.position, touch2.position);
                touchDist = lastDist - newDist;
                lastDist = newDist;
                Zoom(touchDist * 0.01f);
            }
        } else if (Input.GetMouseButton(0)) { //Checking drag evnt.
            if (image.localScale == Vector3.one) {
                if (image.localPosition != Vector3.one) {
                    image.localPosition = new Vector3(Mathf.Lerp(image.localPosition.x, 0, lerpTime), Mathf.Lerp(image.localPosition.y, 0, lerpTime), 1);
                }
            } else {
                image.position = (Input.mousePosition - touchStart);
                image.position = new Vector2(Mathf.Clamp(image.position.x, moveMin.x, moveMax.x), Mathf.Clamp(image.position.y, moveMin.y, moveMax.y));
            }
        }
    }
    Touch touch1;
    Touch touch2;
    float touchDist = 0;
    float lastDist = 0;

    private void DoDoubleTabEvent() {
        if (image.localScale.x < zoomMax) {
            image.localScale = new Vector3(2, 2, 1);
        } else {
            image.localScale = Vector3.one;
        }
    }

    void Zoom(float val) {
        val *= -1;
        image.localScale = new Vector3(image.localScale.x + val, image.localScale.y + val, 1);
        if (image.localScale.x < zoomMin || image.localScale.x > zoomMax) {
            image.localScale = (image.localScale.x < zoomMin) ? new Vector3(zoomMin, zoomMin, 1) : new Vector3(zoomMax, zoomMax, 1);
        }
    }

    public override void Clear() {
        base.Clear();
    }
}
