using UnityEngine;
using UnityEngine.UI;

public class QrManager : EachSceneManager {
    public RawImage qr;
    public Texture2D defaultTexture;

    protected override void Start() {
        base.Start();
        if (global.IsUseDefaultData) {
            qr.texture = defaultTexture;
        } else {
            qr.texture = global.QrTexture;
        }
    }

    public override void Clear() {
        base.Clear();
    }
}
