using UnityEngine;

public class EachSceneManager : MonoBehaviour {
    protected GlobalManager global;
    protected virtual void Start() {
        global = GlobalManager.Instance;
    }

    public virtual void Clear() {
        System.GC.Collect();
        Resources.UnloadUnusedAssets();
    }
}
