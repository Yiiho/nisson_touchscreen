using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrochureManager : EachSceneManager {
    public GameObject prefabBase;
    public Transform imgContainer;
    public Texture2D[] defaultBrochure;
    public Gesture gesture;

    List<GameObject> list;
    protected override void Start() {
        base.Start();
        list = new List<GameObject>();
        if (global.IsUseDefaultData) {
            for (int i = 0; i < defaultBrochure.Length; i++) {
                GameObject go = Instantiate(prefabBase, imgContainer);
                go.GetComponent<RawImage>().texture = defaultBrochure[i];
                int _i = i;
                go.GetComponent<Button>().onClick.AddListener(() => OnPress(_i));
                list.Add(go);
            }
        } else {
            for (int i = 0; i < global.AllPdfImg.Count; i++) {
                GameObject go = Instantiate(prefabBase, imgContainer);
                go.GetComponent<RawImage>().texture = global.AllPdfImg[i];
                int _i = i;
                go.GetComponent<Button>().onClick.AddListener(() => OnPress(_i));
                list.Add(go);
            }
        }
    }

    void OnPress(int index) {
        gesture.index = index;
        gesture.gameObject.SetActive(true);
    }

    public override void Clear() {
        base.Clear();
        for (int i = 0; i < list.Count; i++) {
            int _i = i;
            list[i].GetComponent<Button>().onClick.RemoveListener(() => OnPress(_i));
        }
    }
}
